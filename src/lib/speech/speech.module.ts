import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { Speeches, Speech, SpeechSearchForm, SpeechForm } from './components';
import { SpeechService } from "./speech.service";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [Speech],
  entryComponents: [Speech],
  declarations: [Speeches, Speech, SpeechSearchForm, SpeechForm],
  providers: [SpeechService],
})
export class SpeechModule { }
