import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { SpeechService, ISpeech } from "../speech.service";

@Component({
  selector: 'speeches',
  templateUrl: 'speeches.html'
})

export class Speeches implements OnInit {

  @Input('speeches') speeches:ISpeech[]
  @Output('onSelected') onSelected: EventEmitter<ISpeech> = new EventEmitter<ISpeech>()

  selectedSpeech:ISpeech

  constructor() {}

  ngOnInit() { }

  onSelect(speech:ISpeech){
    this.selectedSpeech = speech
    this.onSelected.emit(speech)
  }

  ngOnChanges({speeches}){
    this.selectedSpeech = undefined
  }
}