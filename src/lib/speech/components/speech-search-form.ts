import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ISubscription } from "rxjs/Subscription";
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';


import { SpeechService } from "../speech.service";

@Component({
  selector: 'speech-search-form',
  templateUrl: 'speech-search-form.html'
})

export class SpeechSearchForm implements OnInit {
  private subscriptions:ISubscription[] = []
  speechSearchForm:FormGroup

  constructor(
    private fb:FormBuilder,
    private service:SpeechService
  ) {
    this.speechSearchForm = fb.group({
      term:['', Validators.required]
    })

    this.subscriptions.push(
      this.speechSearchForm.valueChanges
        .debounceTime(400)
        .distinctUntilChanged()
        .subscribe(value => {
          this.service.setSearchQuery(value.term)
        })
    )

  }

  ngOnInit() { }

  ngOnDestroy(){
    for(const subsciption of this.subscriptions){
      subsciption.unsubscribe()
    }
  }
}