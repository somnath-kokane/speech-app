import { Component, OnInit, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { SpeechService, ISpeech } from "../speech.service";

@Component({
  selector: 'speech-form',
  templateUrl: 'speech-form.html'
})

export class SpeechForm implements OnInit {

  @Input('speech') speech:ISpeech
  @Output('onDeleted') onDeleted:EventEmitter<ISpeech> = new EventEmitter<ISpeech>()
  @Output('onShared') onShared:EventEmitter<ISpeech> = new EventEmitter<ISpeech>()
  @Output('onSaved') onSaved:EventEmitter<ISpeech> = new EventEmitter<ISpeech>()
  speechForm:FormGroup

  constructor(
    private fb:FormBuilder,
    private service:SpeechService
  ) { 
    
  }

  ngOnInit() { }

  buildForm(speech:ISpeech){
    this.speechForm = this.fb.group({
      title: [speech.title, Validators.required],
      content:[speech.content, Validators.required],
      author: [speech.author],
      keywords: [speech.keywords],
      date: [speech.date]
    })
  }

  ngOnChanges({speech}){
    if(speech && speech.currentValue && speech.currentValue !== speech.previousValue){
      this.buildForm(speech.currentValue)
    }
    
  }

  onSubmit(){
    const value = this.speechForm.value
    if(this.speech.id){
      this.service.updateSpeech(this.speech.id, value)
    } else {
      this.speech = this.service.addSpeech(value)
    }
    this.onSaved.emit(this.speech)
  }

  onDelete(){
    if(this.speech.id){
      this.service.deleteSpeech(this.speech.id)
      this.onDeleted.emit(this.speech)
    }
  }

  toggleShare(){
    
  }
}