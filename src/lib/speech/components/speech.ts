import { Component, OnInit } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";

import { ISpeech, SpeechService } from "../speech.service";

@Component({
  selector: 'speech',
  templateUrl: 'speech.html'
})

export class Speech implements OnInit {
  subscriptions: ISubscription[] = []
  speeches: ISpeech[]
  speech:ISpeech

  constructor(
    private service: SpeechService
  ) {
    this.resetSpeech()
    this.subscriptions.push(
      this.service.searchQuery.subscribe(value => {
        this.speeches = this.service.search(value)
        this.speech = undefined
      }),
      this.service.speeches.subscribe(speeches => {
        this.speeches = speeches
      })
    )

  }

  resetSpeech(){
    this.speech = {
      title: '',
      content: '',
      keywords: '',
      author: '',
      date: new Date
    }
  }

  onAdd(){
    this.resetSpeech()
  }

  onDelete(speech:ISpeech){
    this.resetSpeech()
  }

  onSave(speech:ISpeech){
    this.speech = speech
  }

  ngOnInit() { 
    // this.resetSpeech()
  }

  onSelected(speech:ISpeech){
    this.speech = speech
  }

  ngOnDestroy(){
    for(const subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }
}