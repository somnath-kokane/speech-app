import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

export interface ISpeech {
  id?:number
  title:string
  keywords:string
  content:string
  author:string
  date:Date
}


const SPEECHES: ISpeech[] = [
  {id: 1, title: 'SP1', content: 'SPC1', keywords: '', author: '', date: new Date},
  {id: 2, title: 'SP2', content: 'SPC2', keywords: '', author: '', date: new Date},
  {id: 3, title: 'SP3', content: 'SPC3', keywords: '', author: '', date: new Date},
]

@Injectable()
export class SpeechService {

  private nextId:number
  private searchQuerySource:Subject<any>
  speeches:BehaviorSubject<ISpeech[]>
  searchQuery:Observable<string>

  constructor(

  ) {
    this.speeches = new BehaviorSubject<ISpeech[]>(SPEECHES)
    this.searchQuerySource = new Subject<string>()
    this.searchQuery = this.searchQuerySource.asObservable()
    this.nextId = SPEECHES.length
  }

  setSearchQuery(query:string){
    this.searchQuerySource.next(query)
  }

  search(query){
    const speeches = this.speeches.value || []
    if(Boolean(query) === false){
      return speeches
    }
    return speeches.filter(speech => {
      const regex = new RegExp(query, 'ig')
      return regex.test(speech['title'])
    })
  }

  saveSpeech(speech:ISpeech){
    if(speech.id){
      this.updateSpeech(speech.id, speech)
    } else {
      this.addSpeech(speech)
    }
  }

  addSpeech(speech:ISpeech){
    let id = ++this.nextId
    const speeches = this.speeches.value || []
    speech.id = id
    speeches.push(speech)
    this.speeches.next([...speeches])
    return speech
  }

  updateSpeech(id, speech:ISpeech){
    const speeches = this.speeches.value || []
    let origSpeech = speeches.find(value => value.id === id)
    if(Boolean(origSpeech)){
      Object.assign(origSpeech, speech)
      this.speeches.next([...speeches])
    }
    return origSpeech
  }

  deleteSpeech(id){
    const speeches = this.speeches.value || []
    let index = speeches.findIndex(value => value.id === id)
    if(index !== -1){
      speeches.splice(index, 1)
      this.speeches.next([...speeches])
    }
  }

}